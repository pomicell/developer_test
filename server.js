var Express = require('express');
var Path = require('path');
var fs = require('fs');

// App
var app = Express();

// Handling Calc API
app.get('/Calc', function (req, res) {
    // Add your code here

    // Notify calc process finished
    res.send("OK");
});

// Add you Analyze API function here: 

// Handling Load API
app.get('/Load', function (req, res) {
    // Change the following code to use your analyze.csv file and convert it into json
    var content = fs.readFileSync("./results.example.json");

    // Send the JSON back to client
    res.send(content);
});

// sending the main page of the app
app.get('/', function (req, res) {
    res.sendFile(Path.join(__dirname, './index.html'));
});

// Listen to port 8080, you can browse to 127.0.0.1:8080 
app.listen(8080, function(){
    console.log("Listening on port 8080, open 127.0.0.1:8080 to view website");
});
